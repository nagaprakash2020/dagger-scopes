package com.reveal.daggerscopes

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class FragmentA1 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_a1, container, false)
        val button = view.findViewById<Button>(R.id.button)

        button.setOnClickListener{
            goToFragmentA2()
        }
        return view
    }

    companion object {

        @JvmStatic
        fun newInstance() = FragmentA1().apply {}
    }

    private fun goToFragmentA2() {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.container, FragmentA2.newInstance())

        transaction?.addToBackStack("Frag2")?.commit()
    }
}