package com.reveal.daggerscopes

import android.app.Application
import com.reveal.daggerscopes.di.DaggerAppComponent

class DaggerScopeApp: Application() {

    // Reference to the application graph that is used across the whole app
    val appComponent = DaggerAppComponent.create()
}